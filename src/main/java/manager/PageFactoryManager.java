package manager;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {
    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public WomanSectionPage getWomanSectionPage() {
        return new WomanSectionPage(driver);
    }

    public JeansPage getJeansPage() {
        return new JeansPage(driver);
    }

    public ProductPage getProductPage() {
        return new ProductPage(driver);
    }

    public SignInPage getSignInPage() {
        return new SignInPage(driver);
    }

    public HelpPage getHelpPage() {
        return new HelpPage(driver);
    }

    public OutletPage getOutletPage() {
        return new OutletPage(driver);
    }
}
