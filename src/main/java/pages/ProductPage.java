package pages;

import net.bytebuddy.asm.Advice;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage {
    @FindBy(xpath = "//button[@aria-label='Add to bag']")
    private WebElement addToBagButton;
    @FindBy(xpath = "//span[@id='selectSizeError']")
    private WebElement errorLine;
    @FindBy(xpath = "//span[@class='product-colour']")
    private WebElement colorText;
    @FindBy(xpath = "//div[@class='_25L--Pi']")
    private WebElement currencyChanger;
    @FindBy(xpath = "//select[@id='currency']")
    private WebElement currency;
    @FindBy(xpath = "//option[@value='2']")
    private WebElement value;
    @FindBy(xpath = "//button[normalize-space()='Update Preferences']")
    private WebElement update;
    @FindBy(xpath = "//span[@class='current-price']")
    private WebElement price;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnAddToBag() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", addToBagButton);
    }

    public String getErrorText() {
        return errorLine.getText();
    }

    public String getColorText() {
        return colorText.getText();
    }
    public void clickOnCurrencyChanger(){
        currencyChanger.click();
    }
    public void clickOnCurrency(){
        currency.click();
    }
    public void selectValue(){
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", value);
    }
    public void setUpdate(){
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", update);
    }
    public String priceText(){
        return price.getText();
    }
}
