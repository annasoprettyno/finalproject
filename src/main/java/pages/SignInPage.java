package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {
    @FindBy(xpath = "//input[@id='signin']")
    private WebElement signInButton;
    @FindBy(xpath = "//span[@id='EmailAddress-error']")
    private WebElement emailAddressError;
    @FindBy(xpath = "//span[@id='Password-error']")
    private WebElement passwordError;

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnSignInButton() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", signInButton);
    }

    public String emailAddressError() {
        return emailAddressError.getText();
    }

    public String passwordError() {
        return passwordError.getText();
    }

}
