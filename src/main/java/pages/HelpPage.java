package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HelpPage extends BasePage {

    @FindBy(xpath = "//h3[contains(text(),'Delivery')]")
    private WebElement deliveryButton;
    @FindBy(xpath = "//h1[contains(text(),'Delivery')]")
    private WebElement deliveryMainText;

    public HelpPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnDeliveryButton() {
        deliveryButton.click();
    }

    public String getDeliveryMainText() {
        return deliveryMainText.getText();
    }
}
