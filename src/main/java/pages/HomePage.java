package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {
    private String Jeans = "jeans";

    @FindBy(xpath = "//a[@class='_24SZgSx _6DZZlcW']")
    private WebElement mainLogo;
    @FindBy(xpath = "//a[@id='women-floor']")
    private WebElement womanSection;
    @FindBy(xpath = "//a[@id='men-floor']")
    private WebElement manSection;
    @FindBy(xpath = "//input[@id='chrome-search']")
    private WebElement searchLine;
    @FindBy(xpath = "//button[@data-testid='myAccountIcon']")
    private WebElement accountButton;
    @FindBy(xpath = "//a[@aria-label='Saved Items']")
    private WebElement savedItemsButton;
    @FindBy(xpath = "//span[@type='bagUnfilled']")
    private WebElement cartButton;
    @FindBy(xpath = "//div[@class='_25L--Pi']")
    private WebElement languageButton;
    @FindBy(xpath = "//h1[@class='mu__imageWrap']//picture//img")
    private WebElement mainPicture;
    @FindBy(xpath = "//a[@href='/women/?ctaref=HP|gen|top|women']")
    private WebElement mainPictureWomanButton;
    @FindBy(xpath = "//a[@href='/men/?ctaref=HP|gen|top|men']")
    private WebElement mainPictureManButton;
    @FindBy(xpath = "//span[normalize-space()='This is ASOS']")
    private WebElement mainPictureText;
    @FindBy(xpath = "//div[@id='myaccount-dropdown']")
    private WebElement myAccountDropdown;
    @FindBy(xpath = "//a[@data-testid='myaccount-link']")
    private WebElement myAccountButton;
    @FindBy(xpath = "//a[@data-testid='help']")
    private WebElement helpButton;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    public boolean isMainLogoClickable() {
        return mainLogo.isEnabled();
    }

    public boolean isWomanSectionButtonClickable() {
        return womanSection.isEnabled();
    }

    public boolean isManSectionButtonClickable() {
        return manSection.isEnabled();
    }

    public boolean isSearchLineDisplayed() {
        return searchLine.isDisplayed();
    }

    public boolean isAccountButtonClickable() {
        return accountButton.isEnabled();
    }

    public boolean isSavedButtonClickable() {
        return savedItemsButton.isEnabled();
    }

    public boolean isCartButtonClickable() {
        return cartButton.isEnabled();
    }

    public String getLanguage() {
        return languageButton.getText();
    }

    public boolean isMainPictureDisplayed() {
        return mainPicture.isDisplayed();
    }

    public boolean isMainPictureWomanButtonClickable() {
        return mainPictureWomanButton.isEnabled();
    }

    public boolean isMainPictureManButtonClickable() {
        return mainPictureManButton.isEnabled();
    }

    public String getMainPictureText() {
        return mainPictureText.getText();
    }

    public void clickOnWomanSectionButton() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", womanSection);
    }

    public void sendTextToSearchLine() {
        searchLine.sendKeys(Jeans, Keys.ENTER);
    }

    public void clickOnAccountButton() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", accountButton);
    }

    public boolean isMyAccountDropdownVisible() {
        return myAccountDropdown.isDisplayed();
    }

    public void clickOnMyAccountButton() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", myAccountButton);
    }

    public void clickOnHelpButton() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", helpButton);
    }


}
