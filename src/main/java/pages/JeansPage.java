package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;


public class JeansPage extends BasePage {

    @FindBy(xpath = "//a[@data-testid='asoslogo']")
    private WebElement mainLogoJeansPage;
    @FindBy(xpath = "//a[@aria-label='COLLUSION x000 Unisex 90s straight leg jeans in mid wash blue; Price: £25.00']")
    private WebElement jeansProduct;
    @FindBy(xpath = "//div[contains(text(),'Colour')]")
    private WebElement colorSelecter;
    @FindBy(xpath = "//label[@for='base_colour_4']")
    private WebElement blackColor;
    @FindBy(xpath = "//a[@aria-label='Missguided riot recycled mom jean in black; Price: £25.00']")
    private WebElement blackColorJeans;

    public JeansPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnMainLogo() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", mainLogoJeansPage);
    }

    public void clickOnJeansProduct() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", jeansProduct);
    }

    public void clickOnColorSelecter() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", colorSelecter);
    }

    public void clickOnBlackColor() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", blackColor);
    }

    public void clickOnBlackColorJeans() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", blackColorJeans);
    }


}
