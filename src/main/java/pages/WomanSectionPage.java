package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class WomanSectionPage extends BasePage {

    @FindBy(xpath = "//a[normalize-space()='MEN']")
    private WebElement manSection;
    @FindBy(xpath = "//*[@id=\"chrome-sticky-header\"]/div[2]/div[1]/nav/div/div/button[11]")
    private WebElement outlet;
    @FindBy(xpath = "//*[@id=\"a92844a0-3b5b-41b6-ad85-9ca81f5c24f1\"]/div/div[2]/ul/li[1]/ul/li[1]/a")
    private WebElement viewAll;
    @FindBy(xpath = "//div[@class='salesBanner-header__textContainer is-medium']")
    private WebElement salesBanner;
    @FindBy(xpath = "//h1[text()=\"Women's Sale\"]")
    private WebElement salesSectionMainText;

    public WomanSectionPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnManSection() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", manSection);
    }

    public void clickOnOutlet() {
        Actions actions = new Actions(driver);
        actions.moveToElement(outlet);
        actions.perform();
        outlet.click();
    }

    public void clickOnViewAll() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", viewAll);
    }
    public void clickOnSalesBanner() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", salesBanner);
    }
    public String getSectionName(){
        return salesSectionMainText.getText();
    }

}
