package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.FindBy;
import pages.*;


import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {
    @FindBy(xpath = "//p[normalize-space()='Missguided mom jean with seam detail in black']")
    private WebElement blackJeans;
    @FindBy(xpath = "//h1[text()='Preferences']")
    private WebElement preferencesWindow;
    private static final long DEFAULT_TIMEOUT = 60;
    private final String EXPECTED_LANGUAGE_TEXT = "";
    private final String MAIN_PICTURE_TEXT = "This is ASOS";
    private final String ADD_TO_BAG_ERROR = "Please select from the available colour and size options";
    private final String EMAIL_ERROR = "Oops! You need to type your email here";
    private final String PASSWORD_ERROR = "Hey, we need a password here";
    private final String DELIVERY_MAIN_TEXT = "DELIVERY";
    private final String SALES_SECTION_NAME = "Women's Sale";
    private final String COLOR_TEXT = "Black";
    private final String CURRENCY = "£";
    private final String CURRENT_URL_WOMAN = "women";
    private final String CURRENT_URL_MAN = "men";
    private final String CURRENT_URL_DELIVERY = "delivery";
    private final String CURRENT_URL_OUTLET = "outlet";
    private final String CURRENT_URL_HOME_PAGE = "https://www.asos.com/";


    WebDriver driver;
    HomePage homePage;
    WomanSectionPage womanSectionPage;
    PageFactoryManager pageFactoryManager;
    JeansPage jeansPage;
    ProductPage productPage;
    SignInPage signInPage;
    HelpPage helpPage;
    OutletPage outletPage;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User checks main logo visibility")
    public void checkIsMainLogoClickable() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isMainLogoClickable());
    }

    @And("User checks woman section button")
    public void checkIsWomanSectionButtonClickable() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isWomanSectionButtonClickable());
    }

    @And("User checks man section button")
    public void checkIsManSectionButtonClickable() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isManSectionButtonClickable());
    }

    @And("User checks search line")
    public void checkIsSearchLineDisplayed() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isSearchLineDisplayed());
    }

    @And("User checks account button")
    public void checkIsAccountButtonClickable() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isAccountButtonClickable());
    }

    @And("User checks saved button")
    public void checkIsSavedButtonClickable() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isSavedButtonClickable());
    }

    @And("User checks cart button")
    public void checkIsCartButtonClickable() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isCartButtonClickable());
    }

    @And("User checks language of the site")
    public void checkLanguageOfTheSite() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertEquals(homePage.getLanguage(), EXPECTED_LANGUAGE_TEXT);
    }

    @And("User checks the main picture")
    public void checkTheMainPictureIsDisplayed() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isMainPictureDisplayed());
    }

    @And("User checks the main picture woman button")
    public void checkTheMainPictureWomanButtonClickable() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isMainPictureWomanButtonClickable());
    }

    @And("User checks the main picture man button")
    public void checkTheMainPictureManButtonClickable() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(homePage.isMainPictureManButtonClickable());
    }

    @And("User checks the main picture text")
    public void checkTheMainPictureText() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertEquals(homePage.getMainPictureText(), MAIN_PICTURE_TEXT);
    }

    @When("User clicks on woman section button")
    public void womanSectionButtonClick() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.clickOnWomanSectionButton();
    }

    @And("User has correct url")
    public void getCorrectURL() {
        assertTrue(driver.getCurrentUrl().contains(CURRENT_URL_WOMAN));
    }

    @Then("User clicks on man section button")
    public void clickOnManSection() {
        womanSectionPage = pageFactoryManager.getWomanSectionPage();
        womanSectionPage.clickOnManSection();
    }

    @And("User has correct man section url")
    public void getTheCorrectManSectionUrl() {
        assertTrue(driver.getCurrentUrl().contains(CURRENT_URL_MAN));
    }

    @And("User send 'jeans' to search line")
    public void sendTextToSearchLine() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.sendTextToSearchLine();
    }

    @When("User clicks on a main logo")
    public void clickOnMainLogo() {
        jeansPage = pageFactoryManager.getJeansPage();
        jeansPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        jeansPage.clickOnMainLogo();
    }

    @Then("User has correct main url")
    public void getErrorText() {
        assertEquals(driver.getCurrentUrl(), (CURRENT_URL_HOME_PAGE));
    }

    @When("User clicks on product")
    public void clickOnJeansProduct() {
        jeansPage = pageFactoryManager.getJeansPage();
        jeansPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        jeansPage.clickOnJeansProduct();
    }

    @And("User clicks on add to bag")
    public void clickOnAddToBag() {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        productPage.clickOnAddToBag();
    }

    @Then("User has an error")
    public void getAddToCartError() {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertEquals(ADD_TO_BAG_ERROR, productPage.getErrorText());
    }

    @When("User clicks on account button")
    public void clickOnAccountButton() {
        homePage = pageFactoryManager.getHomePage();
        homePage.clickOnAccountButton();
    }

    @And("User clicks on my account button")
    public void clickOnMyAccountButton() {
        homePage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        homePage.clickOnMyAccountButton();
    }

    @And("User clicks on sign in button")
    public void clickOnSignInButton() {
        signInPage = pageFactoryManager.getSignInPage();
        signInPage.clickOnSignInButton();
    }

    @Then("User has email error")
    public void getEmailError() {
        signInPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        assertEquals(EMAIL_ERROR, signInPage.emailAddressError());
    }

    @And("User has password error")
    public void getPasswordError() {
        assertEquals(PASSWORD_ERROR, signInPage.passwordError());
    }

    @When("User clicks on help button")
    public void clickOnHelpButton() {
        homePage.clickOnHelpButton();
    }

    @And("User clicks on delivery button")
    public void clickOnDeliveryButton() {
        helpPage = pageFactoryManager.getHelpPage();
        helpPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        helpPage.clickOnDeliveryButton();
    }

    @Then("User has correct delivering url")
    public void getCurrentDeliveryUrl() {
        helpPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(driver.getCurrentUrl().contains(CURRENT_URL_DELIVERY));
    }

    @And("User has correct main text")
    public void getCorrectMainTextDelivery() {
        assertEquals(helpPage.getDeliveryMainText(), DELIVERY_MAIN_TEXT);
    }

    @And("User clicks on outlet button")
    public void clickOnOutletButton() {
        womanSectionPage = pageFactoryManager.getWomanSectionPage();
        womanSectionPage.clickOnOutlet();
    }

    @Then("User has correct outlet url")
    public void getCurrentOutletUrl() {
        outletPage = pageFactoryManager.getOutletPage();
        assertTrue(driver.getCurrentUrl().contains(CURRENT_URL_OUTLET));
    }

    @And("User clicks on view all button")
    public void clickOnViewAllButton() {
        womanSectionPage = pageFactoryManager.getWomanSectionPage();
        womanSectionPage.clickOnViewAll();
    }

    @And("User clicks on select color button")
    public void clickOnSelectColorButton() {
        jeansPage = pageFactoryManager.getJeansPage();
        jeansPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        jeansPage.clickOnColorSelecter();
    }

    @And("User selects black color")
    public void clickOnBlackColor() {
        jeansPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        jeansPage.clickOnBlackColor();
    }

    @And("User click on a black product")
    public void clickOnBlackJeans() {
        jeansPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        jeansPage.clickOnBlackColorJeans();
    }

    @Then("Color equals to black")
    public void colorEqualsToBlack() {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertEquals(COLOR_TEXT, productPage.getColorText());
    }
    @And("User clicks on sale banner")
    public void clickOnSalesBanner(){
        womanSectionPage = pageFactoryManager.getWomanSectionPage();
        womanSectionPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        womanSectionPage.clickOnSalesBanner();
    }
    @Then("User has woman sales section")
    public void getSalesSectionText(){
        womanSectionPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertEquals(SALES_SECTION_NAME,womanSectionPage.getSectionName());
    }
    @And("User clicks on currencyChanger")
    public void clickOnCurrencyChanger(){
        productPage = pageFactoryManager.getProductPage();
        productPage.clickOnCurrencyChanger();
    }
    @And("User clicks on currency")
    public void clickOnCurrency(){
        productPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        productPage.clickOnCurrency();
    }
    @And("User select value")
    public void setValue(){
        productPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        productPage.selectValue();
    }
    @And("User clicks on update")
    public void clickOnUpdate(){
        productPage.waitForAjaxToComplete(DEFAULT_TIMEOUT);
        productPage.setUpdate();
        driver.navigate().refresh();
    }
    @Then("User has value updated")
    public void getCurrency(){
        productPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(productPage.priceText().contains(CURRENCY));
    }

    @After
    public void tearDown() {
        driver.close();
    }

}
