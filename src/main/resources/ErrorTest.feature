Feature: ErrorTest
  As a user, I want to test all error functionality
  So that I can be sure that my exceptions work correctly

  Scenario: Check the add to bad error

    Given User opens 'https://www.asos.com/' page
    And User send 'jeans' to search line
    When User clicks on product
    And User clicks on add to bag
    Then User has an error

  Scenario: Check the log in error

    Given User opens 'https://www.asos.com/' page
    When User clicks on account button
    And User clicks on my account button
    And User clicks on sign in button
    Then User has email error
    And User has password error