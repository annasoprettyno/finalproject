Feature: SmokeTest
  As a user, I want to test all HomePage functionality
  So that I can be sure that my main screen components work correctly

  Scenario Outline: Check main component of Home page

    Given User opens '<home page>' page
    And User checks main logo visibility
    And User checks woman section button
    And User checks man section button
    And User checks search line
    And User checks account button
    And User checks saved button
    And User checks cart button
    And User checks language of the site
    And User checks the main picture
    And User checks the main picture woman button
    And User checks the main picture man button
    Then User checks the main picture text

    Examples:
      | home page             |
      | https://www.asos.com/ |


  Scenario Outline: Check man and woman button

    Given User opens '<home page>' page
    When User clicks on woman section button
    And User has correct url
    Then User clicks on man section button
    And User has correct man section url


    Examples:
      | home page             |
      | https://www.asos.com/ |


  Scenario: Check if the main logo return to home page

    Given User opens 'https://www.asos.com/' page
    And User send 'jeans' to search line
    When User clicks on a main logo
    Then User has correct main url


  Scenario: Check the help button and delivery button

    Given User opens 'https://www.asos.com/' page
    When User clicks on help button
    And User clicks on delivery button
    Then User has correct delivering url
    And User has correct main text


  Scenario: Check the outlet button

    Given User opens 'https://www.asos.com/' page
    When User clicks on woman section button
    And User clicks on outlet button
    And User clicks on view all button
    Then User has correct outlet url


  Scenario: Check the women sales section

    Given User opens 'https://www.asos.com/' page
    When User clicks on woman section button
    And User clicks on sale banner
    Then User has woman sales section