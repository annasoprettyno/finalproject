Feature: FunctionalTest
  As a user, I want to test functional part of the site
  So that I can be sure that my functions work correctly


  Scenario: Check the color filter

    Given User opens 'https://www.asos.com/' page
    When User send 'jeans' to search line
    And User clicks on select color button
    And User selects black color
    And User click on a black product
    Then Color equals to black


  Scenario: Check the currency changing

    Given User opens 'https://www.asos.com/' page
    When User send 'jeans' to search line
    And User clicks on product
    And User clicks on currencyChanger
    And User clicks on currency
    And User select value
    And User clicks on update
    Then User has value updated